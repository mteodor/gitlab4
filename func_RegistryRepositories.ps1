# Container Registry API
# https://docs.gitlab.com/ee/api/container_registry.html

# get a list of project registry repositories
function Get-GitlabRegistryRepositories( [Parameter(Mandatory=$true)] [string] $project
                                       , [Parameter(Mandatory=$false)][switch] $tags
                                       , [Parameter(Mandatory=$false)][switch] $tags_count
                                       )
{
  [string] $GAPI_REGISTRY_REPOSITORIES = "$CI_API_V4_URL/projects/$project/registry/repositories?per_page=100"

  if ($tags) {
    $GAPI_REGISTRY_REPOSITORIES += "&tags=true"
  }

  if ($tags_count) {
    $GAPI_REGISTRY_REPOSITORIES += "&tags_count=true"
  }

  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_REGISTRY_REPOSITORIES)
}

# get a single repository by ID
# (requires version 13.6)
function Get-GitlabRegistryRepository( [Parameter(Mandatory=$true)] [string] $id
                                     , [Parameter(Mandatory=$false)][switch] $tags
                                     , [Parameter(Mandatory=$false)][switch] $tags_count
                                     )
{
  [string] $GAPI_REGISTRY_REPOSITORIES_ID = "$CI_API_V4_URL/registry/repositories/${id}?size=true"

  if ($tags) {
    $GAPI_REGISTRY_REPOSITORIES_ID += "&tags=true"
  }

  if ($tags_count) {
    $GAPI_REGISTRY_REPOSITORIES_ID += "&tags_count=true"
  }

  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_REGISTRY_REPOSITORIES_ID -method GET)
}

# get a single repository by name
function Get-GitlabRegistryRepositoryByName( [Parameter(Mandatory=$true)] [string] $project
                                           , [Parameter(Mandatory=$true)] [string] $name
                                           )
{
  $prcrr = @(Get-GitlabRegistryRepositories -project $project)
  foreach ($crr in $prcrr) {
    if ($crr.name -eq $name) {
      return (Get-GitlabRegistryRepository -id $crr.id -tags -tags_count)
    }
  }

  return @()
}

# delete a registry repository by ID
function Remove-GitlabRegistryRepository( [Parameter(Mandatory=$true)] [string] $project
                                        , [Parameter(Mandatory=$true)] [string] $id
                                        )
{
  [string] $GAPI_REGISTRY_REPOSITORIES_ID = "$CI_API_V4_URL/projects/$project/registry/repositories/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_REGISTRY_REPOSITORIES_ID -method DELETE)
}

# delete a registry repository by name
function Remove-GitlabRegistryRepositoryByName( [Parameter(Mandatory=$true)] [string] $project
                                              , [Parameter(Mandatory=$true)] [string] $name
                                              )
{
  $prcrr = @(Get-GitlabRegistryRepositories -project $project)
  foreach ($crr in $prcrr) {
    if ($crr.name -eq $name) {
      return (Invoke-RestMethod -headers $GLPT -uri $crr.delete_api_path -method DELETE)
    }
  }

  return 0
}
