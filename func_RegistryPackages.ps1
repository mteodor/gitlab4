# ---------------------------------------------------------------------
# Packages Registry API
# https://docs.gitlab.com/ee/api/packages.html

# get a list of project packages
# (requires version 12.9)
function Get-GitlabRegistryPackages( [Parameter(Mandatory=$true)] [string] $project
                                   , [Parameter(Mandatory=$false)][string] $package_type
                                   , [Parameter(Mandatory=$false)][string] $package_name
                                   , [Parameter(Mandatory=$false)][string] $order_by
                                   , [Parameter(Mandatory=$false)][string] $status
                                   , [Parameter(Mandatory=$false)][string] $sort
                                   )
{
  [string] $GAPI_PACKAGES = "$CI_API_V4_URL/projects/$project/packages?per_page=100"

  if ($PSBoundParameters.ContainsKey('package_type')) {
    $GAPI_PACKAGES += "&package_type=$package_type"
  }

  if ($PSBoundParameters.ContainsKey('package_name')) {
    $GAPI_PACKAGES += "&package_name=$package_name"
  }

  if ($PSBoundParameters.ContainsKey('order_by')) {
    $GAPI_PACKAGES += "&order_by=$order_by"
  }

  if ($PSBoundParameters.ContainsKey('status')) {
    $GAPI_PACKAGES += "&status=$status"
  }

  if ($PSBoundParameters.ContainsKey('sort')) {
    $GAPI_PACKAGES += "&sort=$sort"
  }

  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_PACKAGES)
}

# get a single package
function Get-GitlabRegistryPackage( [Parameter(Mandatory=$true)] [string] $project
                                  , [Parameter(Mandatory=$true)] [string] $id
                                  )
{
  [string] $GAPI_PACKAGE_ID = "$CI_API_V4_URL/projects/$project/packages/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_PACKAGE_ID -method GET)
}

# delete a single package
function Remove-GitlabRegistryPackage( [Parameter(Mandatory=$true)] [string] $project
                                     , [Parameter(Mandatory=$true)] [string] $id
                                     )
{
  [string] $GAPI_PACKAGE_ID = "$CI_API_V4_URL/projects/$project/packages/$id"
  Invoke-RestMethod -headers $GLPT -uri $GAPI_PACKAGE_ID -method DELETE | Out-Null
}
