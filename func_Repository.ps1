# ---------------------------------------------------------------------
# Repository files API
# https://docs.gitlab.com/ee/api/repository_files.html

# get raw file from repository
function Get-GitlabRepositoryFileRaw( [Parameter(Mandatory=$true)] [string] $project
                                    , [Parameter(Mandatory=$true)] [string] $file_path
                                    , [Parameter(Mandatory=$false)][string] $ref
                                    , [Parameter(Mandatory=$false)][switch] $lfs
                                    )
{
  $file_path = [uri]::EscapeDataString($file_path)
  [string] $GAPI_FILES_ID = "$CI_API_V4_URL/projects/$project/repository/files/$file_path/raw"

  if (![string]::IsNullOrWhiteSpace($ref)) {
    $ref = [uri]::EscapeDataString($ref)
    $GAPI_FILES_ID += "?ref=$ref"

    if ($lfs) {
      $GAPI_FILES_ID += "&lfs=true"
    }
  }

  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_FILES_ID -method GET)
}

# modify an existing file from repository
# (requires version 14.0)
function Set-GitlabRepositoryFileRaw( [Parameter(Mandatory=$true)] [string] $project
                                    , [Parameter(Mandatory=$true)] [string] $branch
                                    , [Parameter(Mandatory=$true)] [string] $content
                                    , [Parameter(Mandatory=$true)] [string] $file_path
                                    , [Parameter(Mandatory=$true)] [string] $message
                                    , [Parameter(Mandatory=$false)][string] $author_email
                                    , [Parameter(Mandatory=$false)][string] $author_name
                                    , [Parameter(Mandatory=$false)][string] $start_branch
                                    , [Parameter(Mandatory=$false)][switch] $execute_filemode
                                    )
{
  [string] $content_coded = $content -replace "\r\n", "`n"
  $body_json = @{ "branch"         = "$branch";
                  "commit_message" = "$message";
                  "content"        = "$content_coded";
                }

  if (![string]::IsNullOrWhiteSpace($start_branch)) {
    $body_json += @{ "start_branch" = "$start_branch" }
  }

  if (![string]::IsNullOrWhiteSpace($author_email)) {
    $body_json += @{ "author_email" = "$author_email" }
  }

  if (![string]::IsNullOrWhiteSpace($author_name)) {
    $body_json += @{ "author_name" = "$author_name" }
  }

  if ($execute_filemode) {
    $body_json += @{ "execute_filemode" = "true" }
  }

  [string] $body = $body_json | ConvertTo-Json -depth 10
           $file_path = [uri]::EscapeDataString($file_path)
  [string] $GAPI_FILES_ID = "$CI_API_V4_URL/projects/$project/repository/files/$file_path"

  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_FILES_ID -method PUT -ContentType 'application/json' -body $body)
}

# modify an existing file from repository
# (requires version 14.0)
function Edit-GitlabRepositoryFile( [Parameter(Mandatory=$true)] [string] $project
                                  , [Parameter(Mandatory=$true)] [string] $branch
                                  , [Parameter(Mandatory=$true)] [string] $file_path
                                  , [Parameter(Mandatory=$true)] [string] $message
                                  , [Parameter(Mandatory=$true)] [string] $old_value
                                  , [Parameter(Mandatory=$true)] [string] $new_value
                                  , [Parameter(Mandatory=$false)][string] $author_email
                                  , [Parameter(Mandatory=$false)][string] $author_name
                                  , [Parameter(Mandatory=$false)][string] $start_branch
                                  )
{
  [string] $source_branch = if (![string]::IsNullOrWhiteSpace($start_branch)) { $start_branch } else { $branch }
  [string] $raw_file = Get-GitlabRepositoryFileRaw -project $project   `
                                                   -ref $source_branch `
                                                   -file_path $file_path

  [string] $new_content = $raw_file -replace $old_value, $new_value
  return (Set-GitlabRepositoryFileRaw -project $project           `
                                      -branch $branch             `
                                      -content $new_content       `
                                      -file_path $file_path       `
                                      -message $message           `
                                      -author_email $author_email `
                                      -author_name  $author_name  `
                                      -start_branch $start_branch)
}
