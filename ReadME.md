PowerShell module for Gitlab API v4
===================================

This is the source code for pwsh module [**gitlab4**][gallery] published on PowerShell Gallery.

[gallery]: https://www.powershellgallery.com/packages/gitlab4


## Requirements

All functions will authenticate to the GitLab API using a [Personal access token][pat] using the `$GLPT` global variable.
This token must have the required access scope for the operations you define in your pwsh script. At a minimum you should set the `read_api, read_user` scopes.

You MUST declare these global variables in your pwsh script for all helper functions to work.

```powershell
# GitLab requires TLS 1.2+
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# services authentication and authorization
$global:GLPT = @{ 'Private-Token' = $env:GitLab_PrivateToken }

# predefined CI/CD env variables for every Gitlab pipeline
[string] $global:CI_API_V4_URL = $env:CI_API_V4_URL
[string] $global:CI_SERVER_URL = $env:CI_SERVER_URL
```

For Gitlab pipelines you must set the `GitLab_PrivateToken` environment variable.

[pat]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html


### How-To use the pwsh module

```powershell
[string] $proj = 'groupname/subgroup/projectname'

# URL-encoded project path
[string] $proj_path = [uri]::EscapeDataString($proj)

# get all opened merge requests
$mrList = Get-GitlabMergeRequests -project $proj_path -opened
Write-Host "Gitlab project '$proj' has $($mrList.Count) opened merge requests."

# get full info for pipelines and diverged commits count
$mr = Get-GitlabMergeRequest -project $proj_path -iid 42

Write-Host "[$($mr.references.full)] $($mr.title)"
Write-Host "- opened by $($mr.author.name)"
Write-Host "- labels: $($mr.labels -join ', ')"
Write-Host "- source branch: $($mr.source_branch)"
Write-Host "- target branch: $($mr.target_branch)"
Write-Host "- commits behind target branch: $($mr.diverged_commits_count)"
```
