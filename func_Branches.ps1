# ---------------------------------------------------------------------
# Repository branches API
# https://docs.gitlab.com/ee/api/branches.html

# get a list of repository branches from a project
function Get-GitlabBranches( [Parameter(Mandatory=$true)] [string] $project
                           , [Parameter(Mandatory=$false)][string] $search
                           )
{
  [string] $GAPI_BRANCHES = "$CI_API_V4_URL/projects/$project/repository/branches?per_page=100"

  if ($PSBoundParameters.ContainsKey('search')) {
    $GAPI_BRANCHES += "&search=$search"
  }

  return @(Invoke-RestMethod -headers $GLPT -uri $GAPI_BRANCHES -method GET)
}

# get a single repository branch
function Get-GitlabBranch( [Parameter(Mandatory=$true)] [string] $project
                         , [Parameter(Mandatory=$true)] [string] $branch
                         )
{
  [string] $GAPI_BRANCHES_ID = "$CI_API_V4_URL/projects/$project/repository/branches/$branch"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_BRANCHES_ID -method GET)
}

# create a new branch in the repository
function New-GitlabBranch( [Parameter(Mandatory=$true)] [string] $project
                         , [Parameter(Mandatory=$true)] [string] $branch
                         , [Parameter(Mandatory=$false)][string] $ref = 'master'
                         , [Parameter(Mandatory=$false)][string] $author
                         )
{
  [string] $body = @{ "branch" = "$branch";
                      "ref"    = "$ref";
                    } | ConvertTo-Json -depth 10

  [string] $GAPI_BRANCHES = "$CI_API_V4_URL/projects/$project/repository/branches"
  if (![string]::IsNullOrWhiteSpace($author)) {
    $GAPI_BRANCHES += "?sudo=$author"
  }

  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_BRANCHES -method POST -ContentType 'application/json' -body $body)
}

# delete a branch from the repository
function Remove-GitlabBranch( [Parameter(Mandatory=$true)] [string] $project
                            , [Parameter(Mandatory=$true)] [string] $branch
                            )
{
  [string] $GAPI_BRANCHES_ID = "$CI_API_V4_URL/projects/$project/repository/branches/$branch"
  Invoke-RestMethod -headers $GLPT -uri $GAPI_BRANCHES_ID -method DELETE | Out-Null
}
