# ---------------------------------------------------------------------
# Deployments API
# https://docs.gitlab.com/ee/api/deployments.html

# get a list of project deployments
function Get-GitlabDeployments( [Parameter(Mandatory=$true)] [string] $project
                              , [Parameter(Mandatory=$false)][string] $environment
                              , [Parameter(Mandatory=$false)][switch] $blocked
                              , [Parameter(Mandatory=$false)][switch] $canceled
                              , [Parameter(Mandatory=$false)][switch] $created
                              , [Parameter(Mandatory=$false)][switch] $failed
                              , [Parameter(Mandatory=$false)][switch] $running
                              , [Parameter(Mandatory=$false)][switch] $success
                              )
{
  [string] $GAPI_DEPLOYMENTS = "$CI_API_V4_URL/projects/$project/deployments?per_page=100"

  if (![string]::IsNullOrWhiteSpace($environment)) {
    $environment = [uri]::EscapeDataString($environment)
    $GAPI_DEPLOYMENTS += "&environment=$environment"
  }

  if ($blocked) {
    $GAPI_DEPLOYMENTS += "&status=blocked"
  }
  elseif ($canceled) {
    $GAPI_DEPLOYMENTS += "&status=canceled"
  }
  elseif ($created) {
    $GAPI_DEPLOYMENTS += "&status=created"
  }
  elseif ($failed) {
    $GAPI_DEPLOYMENTS += "&status=failed"
  }
  elseif ($running) {
    $GAPI_DEPLOYMENTS += "&status=running"
  }
  elseif ($success) {
    $GAPI_DEPLOYMENTS += "&status=success"
  }

  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_DEPLOYMENTS)
}

# get a single deployment
function Get-GitlabDeployment( [Parameter(Mandatory=$true)] [string] $project
                             , [Parameter(Mandatory=$true)] [string] $id
                             )
{
  [string] $GAPI_DEPLOYMENTS_ID = "$CI_API_V4_URL/projects/$project/deployments/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_DEPLOYMENTS_ID -method GET)
}

# delete a deployment by ID
# (requires version 15.3)
function Remove-GitlabDeployment( [Parameter(Mandatory=$true)] [string] $project
                                , [Parameter(Mandatory=$true)] [string] $id
                                )
{
  [string] $GAPI_DEPLOYMENTS_ID = "$CI_API_V4_URL/projects/$project/deployments/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_DEPLOYMENTS_ID -method DELETE)
}
