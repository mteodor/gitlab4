# save content as JSON from a web request that spans over multiple pages
function Invoke-WebRequestContentToJson( [Parameter(Mandatory=$true)][string] $uri
                                       , [Parameter(Mandatory=$true)]         $headers
                                       )
{
  $webreq = Invoke-WebRequest -headers $headers -uri $uri
  $rclist = $webreq.Content | ConvertFrom-Json

  # sometimes the returned result will span across many pages
  while ($webreq.Headers['X-Next-Page']) {
    [string] $next_page = "$uri&page=$($webreq.Headers['X-Next-Page'])"
    $webreq = Invoke-WebRequest -headers $headers -uri $next_page

    $rcpage  = $webreq.Content | ConvertFrom-Json
    $rclist += $rcpage
  }

  return $rclist
}
