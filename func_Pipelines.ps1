# ---------------------------------------------------------------------
# Pipelines API
# https://docs.gitlab.com/ee/api/pipelines.html

# get a list of project pipelines
function Get-GitlabPipelines( [Parameter(Mandatory=$true)] [string] $project
                            , [Parameter(Mandatory=$false)][string] $status
                            , [Parameter(Mandatory=$false)][string] $ref
                            , [Parameter(Mandatory=$false)][string] $sha
                            , [Parameter(Mandatory=$false)][string] $sort
                            , [Parameter(Mandatory=$false)][string] $source
                            , [Parameter(Mandatory=$false)][string] $username
                            , [Parameter(Mandatory=$false)][string] $before
                            )
{
  [string] $GAPI_PIPELINES = "$CI_API_V4_URL/projects/$project/pipelines?per_page=100"

  if ($PSBoundParameters.ContainsKey('status')) {
    $GAPI_PIPELINES += "&status=$status"
  }

  if ($PSBoundParameters.ContainsKey('ref')) {
    $GAPI_PIPELINES += "&ref=$ref"
  }

  if ($PSBoundParameters.ContainsKey('sha')) {
    $GAPI_PIPELINES += "&sha=$sha"
  }

  if ($PSBoundParameters.ContainsKey('sort')) {
    $GAPI_PIPELINES += "&sort=$sort"
  }

  if ($PSBoundParameters.ContainsKey('source')) {
    $GAPI_PIPELINES += "&source=$source"
  }

  if ($PSBoundParameters.ContainsKey('username')) {
    $GAPI_PIPELINES += "&username=$username"
  }

  if ($PSBoundParameters.ContainsKey('before')) {
    $GAPI_PIPELINES += "&updated_before=$before"
  }

  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_PIPELINES)
}

# get a single pipeline
function Get-GitlabPipeline( [Parameter(Mandatory=$true)] [string] $project
                           , [Parameter(Mandatory=$true)] [string] $id
                           )
{
  [string] $GAPI_PIPELINE_ID = "$CI_API_V4_URL/projects/$project/pipelines/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_PIPELINE_ID -method GET)
}

# delete a single pipeline
# (requires version 11.6)
function Remove-GitlabPipeline( [Parameter(Mandatory=$true)] [string] $project
                              , [Parameter(Mandatory=$true)] [string] $id
                              )
{
  [string] $GAPI_PIPELINE_ID = "$CI_API_V4_URL/projects/$project/pipelines/$id"
  Invoke-RestMethod -headers $GLPT -uri $GAPI_PIPELINE_ID -method DELETE | Out-Null
}

# get all pipeline jobs
function Get-GitlabPipelineJobs( [Parameter(Mandatory=$true)] [string] $project
                               , [Parameter(Mandatory=$true)] [string] $id
                               )
{
  [string] $GAPI_PIPELINE_ID_JOBS = "$CI_API_V4_URL/projects/$project/pipelines/$id/jobs"
  return @(Invoke-RestMethod -headers $GLPT -uri $GAPI_PIPELINE_ID_JOBS -method GET)
}

# retry pipeline jobs with conditions
function Restart-GitlabPipelineJobs( [Parameter(Mandatory=$true)] [string] $project
                                   , [Parameter(Mandatory=$true)] [string] $id
                                   , [Parameter(Mandatory=$false)][string] $name
                                   , [Parameter(Mandatory=$false)][string] $stage
                                   , [Parameter(Mandatory=$false)][string] $status
                                   )
{
  $ppl_jobs = Get-GitlabPipelineJobs -project $project -id $id
  foreach ($job in $ppl_jobs) {
    if (([string]::IsNullOrWhiteSpace($name) -or
          $job.name -cmatch $name) -and
        ([string]::IsNullOrWhiteSpace($stage) -or
          $job.stage -cmatch $stage) -and
        ([string]::IsNullOrWhiteSpace($status) -or
          $job.status -cmatch $status)) {
      Restart-GitlabJob -project $project -id $job.id | Out-Null
    }
  }
}
