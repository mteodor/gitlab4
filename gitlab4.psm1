# =====================================================================
# Gitlab API v4 helper functions

## !! You MUST declare these global vars !!
##
## # GitLab requires TLS 1.2+
## [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
##
## # services authentication and authorization
## $GLPT = @{ 'Private-Token' = $env:GitLab_PrivateToken }
##
## # predefined CI/CD env variables for every Gitlab pipeline
## [string] $global:CI_API_V4_URL = $env:CI_API_V4_URL
## [string] $global:CI_SERVER_URL = $env:CI_SERVER_URL

@( "$PSScriptRoot/func_Branches.ps1"
 , "$PSScriptRoot/func_Commits.ps1"
 , "$PSScriptRoot/func_Deployments.ps1"
 , "$PSScriptRoot/func_Environments.ps1"
 , "$PSScriptRoot/func_Issues.ps1"
 , "$PSScriptRoot/func_Jobs.ps1"
 , "$PSScriptRoot/func_MergeRequests.ps1"
 , "$PSScriptRoot/func_Milestones.ps1"
 , "$PSScriptRoot/func_Notes.ps1"
 , "$PSScriptRoot/func_Pipelines.ps1"
 , "$PSScriptRoot/func_RegistryPackages.ps1"
 , "$PSScriptRoot/func_RegistryRepositories.ps1"
 , "$PSScriptRoot/func_Repository.ps1"
 , "$PSScriptRoot/func_Users.ps1"
 , "$PSScriptRoot/Invoke-WebRequestContentToJson.ps1"
 ) | ForEach-Object { . $_ }
