# ---------------------------------------------------------------------
# Environments API
# https://docs.gitlab.com/ee/api/environments.html

# get a list of project environments
function Get-GitlabEnvironments( [Parameter(Mandatory=$true)] [string] $project
                               , [Parameter(Mandatory=$false)][switch] $available
                               , [Parameter(Mandatory=$false)][switch] $stopping
                               , [Parameter(Mandatory=$false)][switch] $stopped
                               , [Parameter(Mandatory=$false)][string] $name
                               , [Parameter(Mandatory=$false)][string] $search
                               )
{
  [string] $GAPI_ENVIRONMENTS = "$CI_API_V4_URL/projects/$project/environments?per_page=100"

  if ($available) {
    $GAPI_ENVIRONMENTS += "&states=available"
  }
  elseif ($stopping) {
    $GAPI_ENVIRONMENTS += "&states=stopping"
  }
  elseif ($stopped) {
    $GAPI_ENVIRONMENTS += "&states=stopped"
  }

  if ($PSBoundParameters.ContainsKey('name')) {
    $GAPI_ENVIRONMENTS += "&name=$name"
  }
  elseif ($PSBoundParameters.ContainsKey('search')) {
    $GAPI_ENVIRONMENTS += "&search=$search"
  }

  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_ENVIRONMENTS)
}

# get a single environment
function Get-GitlabEnvironment( [Parameter(Mandatory=$true)] [string] $project
                              , [Parameter(Mandatory=$true)] [string] $id
                              )
{
  [string] $GAPI_ENVIRONMENTS_ID = "$CI_API_V4_URL/projects/$project/environments/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_ENVIRONMENTS_ID -method GET)
}

# delete an environment by ID
function Remove-GitlabEnvironment( [Parameter(Mandatory=$true)] [string] $project
                                 , [Parameter(Mandatory=$true)] [string] $id
                                 )
{
  [string] $GAPI_ENVIRONMENTS_ID = "$CI_API_V4_URL/projects/$project/environments/$id"
  Invoke-RestMethod -headers $GLPT -uri $GAPI_ENVIRONMENTS_ID -method DELETE | Out-Null
}
