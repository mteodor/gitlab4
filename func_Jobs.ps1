# ---------------------------------------------------------------------
# Job Artifacts API
# https://docs.gitlab.com/ee/api/job_artifacts.html

# get artifacts of a job
function Get-GitlabJobArtifacts( [Parameter(Mandatory=$true)] [string] $project
                               , [Parameter(Mandatory=$true)] [string] $id
                               )
{
  [string] $GAPI_JOB_ID_ARTIFACTS = "$CI_API_V4_URL/projects/$project/jobs/$id/artifacts"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOB_ID_ARTIFACTS)
}

# delete artifacts of a job
function Remove-GitlabJobArtifacts( [Parameter(Mandatory=$true)] [string] $project
                                  , [Parameter(Mandatory=$true)] [string] $id
                                  )
{
  [string] $GAPI_JOB_ID_ARTIFACTS = "$CI_API_V4_URL/projects/$project/jobs/$id/artifacts"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOB_ID_ARTIFACTS -method DELETE)
}

# delete all project artifacts
function Remove-GitlabArtifacts([Parameter(Mandatory=$true)] [string] $project)
{
  [string] $GAPI_ARTIFACTS = "$CI_API_V4_URL/projects/$project/artifacts"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_ARTIFACTS -method DELETE)
}


# ---------------------------------------------------------------------
# Jobs API
# https://docs.gitlab.com/ee/api/jobs.html

# get a list of project jobs
function Get-GitlabJobs([Parameter(Mandatory=$true)] [string] $project)
{
  [string] $GAPI_JOBS = "$CI_API_V4_URL/projects/$project/jobs?per_page=100"
  return @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_JOBS)
}

# get a single job
function Get-GitlabJob( [Parameter(Mandatory=$true)] [string] $project
                      , [Parameter(Mandatory=$true)] [string] $id
                      )
{
  [string] $GAPI_JOBS_ID = "$CI_API_V4_URL/projects/$project/jobs/$id"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID -method GET)
}

# get log/trace of a specific job
function Get-GitlabJobTrace( [Parameter(Mandatory=$true)] [string] $project
                           , [Parameter(Mandatory=$true)] [string] $id
                           )
{
  [string] $GAPI_JOBS_ID_TRACE = "$CI_API_V4_URL/projects/$project/jobs/$id/trace"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID_TRACE -method GET)
}

# erase log/trace a single job
# (it doesn't remove the job)
function Clear-GitlabJobTrace( [Parameter(Mandatory=$true)] [string] $project
                             , [Parameter(Mandatory=$true)] [string] $id
                             )
{
  [string] $GAPI_JOBS_ID_ERASE = "$CI_API_V4_URL/projects/$project/jobs/$id/erase"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID_ERASE -method POST)
}

# play a single job
function Start-GitlabJob( [Parameter(Mandatory=$true)] [string] $project
                        , [Parameter(Mandatory=$true)] [string] $id
                        )
{
  [string] $GAPI_JOBS_ID_PLAY = "$CI_API_V4_URL/projects/$project/jobs/$id/play"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID_PLAY -method POST)
}

# retry a single job
function Restart-GitlabJob( [Parameter(Mandatory=$true)] [string] $project
                          , [Parameter(Mandatory=$true)] [string] $id
                          )
{
  [string] $GAPI_JOBS_ID_RETRY = "$CI_API_V4_URL/projects/$project/jobs/$id/retry"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID_RETRY -method POST)
}

# cancel a single job
function Stop-GitlabJob( [Parameter(Mandatory=$true)] [string] $project
                       , [Parameter(Mandatory=$true)] [string] $id
                       )
{
  [string] $GAPI_JOBS_ID_CANCEL = "$CI_API_V4_URL/projects/$project/jobs/$id/cancel"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_JOBS_ID_CANCEL -method POST)
}
