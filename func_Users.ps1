# ---------------------------------------------------------------------
# Users API
# https://docs.gitlab.com/ee/api/users.html

# get ID of user
function Get-GitlabUserID([Parameter(Mandatory=$true)][string] $username)
{
  # '0' or white space is used to unassign
  if ($username -cmatch '^[0-9]+$') {
    # we already have an ID
    return $username
  }

  [string] $GAPI_USERS = "$CI_API_V4_URL/users?username=$username"
  $users = Invoke-RestMethod -headers $GLPT -uri $GAPI_USERS -method GET
  if ($users.Length -ne 1) { return 0 }

  return $users[0].id
}
