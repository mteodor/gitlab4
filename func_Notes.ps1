# Notes API
# https://docs.gitlab.com/ee/api/notes.html

# get a single merge request note
function Get-GitlabMergeRequestNote( [Parameter(Mandatory=$true)] [string] $project
                                   , [Parameter(Mandatory=$true)] [string] $mr_iid
                                   , [Parameter(Mandatory=$true)] [string] $prefix
                                   )
{
  [string] $GAPI_MERGE_IID_NOTES = "$CI_API_V4_URL/projects/$project/merge_requests/$mr_iid/notes?per_page=100&sort=asc"
  $mrnotes = @(Invoke-WebRequestContentToJson -headers $GLPT -uri $GAPI_MERGE_IID_NOTES)

  foreach ($note in $mrnotes) {
    if (!$note.system -and !$note.resolvable -and $note.body.StartsWith($prefix)) {
      return $note
    }
  }

  return $null
}

# create a new merge request note
function New-GitlabMergeRequestNote( [Parameter(Mandatory=$true)] [string] $project
                                   , [Parameter(Mandatory=$true)] [string] $mr_iid
                                   , [Parameter(Mandatory=$true)] [string] $note
                                   )
{
  [string] $note_coded = $note -replace "\r\n", "`n"
  [string] $body = @{ "body" = "$note_coded" } | ConvertTo-Json -depth 10

  [string] $GAPI_MERGE_IID_NOTES = "$CI_API_V4_URL/projects/$project/merge_requests/$mr_iid/notes"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_MERGE_IID_NOTES -method POST -ContentType 'application/json' -body $body)
}

# delete a single merge request note
function Remove-GitlabMergeRequestNote( [Parameter(Mandatory=$true)] [string] $project
                                      , [Parameter(Mandatory=$true)] [string] $mr_iid
                                      , [Parameter(Mandatory=$true)] [string] $note_id
                                      )
{
  [string] $GAPI_MERGE_IID_NOTES_ID = "$CI_API_V4_URL/projects/$project/merge_requests/$mr_iid/notes/$note_id"
  Invoke-RestMethod -headers $GLPT -uri $GAPI_MERGE_IID_NOTES_ID -method DELETE | Out-Null
}
