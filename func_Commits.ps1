# ---------------------------------------------------------------------
# Repository commits API
# https://docs.gitlab.com/ee/api/commits.html

# get a list of repository commits from a project
# (requires version 15.0)
function Get-GitlabCommits( [Parameter(Mandatory=$true)] [string] $project
                          , [Parameter(Mandatory=$false)][string] $ref
                          , [Parameter(Mandatory=$false)][string] $author
                          , [Parameter(Mandatory=$false)][string] $path
                          , [Parameter(Mandatory=$false)][string] $order
                          , [Parameter(Mandatory=$false)][string] $since
                          , [Parameter(Mandatory=$false)][string] $until
                          , [Parameter(Mandatory=$false)][switch] $first_parent
                          , [Parameter(Mandatory=$false)][switch] $trailers
                          , [Parameter(Mandatory=$false)][switch] $with_stats
                          )
{
  [string] $GAPI_COMMITS = "$CI_API_V4_URL/projects/$project/repository/commits?per_page=100"

  if ($PSBoundParameters.ContainsKey('ref')) {
    $GAPI_COMMITS += "&ref_name=$ref"
  }

  if ($PSBoundParameters.ContainsKey('author')) {
    $GAPI_COMMITS += "&author=$author"
  }

  if ($PSBoundParameters.ContainsKey('path')) {
    $GAPI_COMMITS += "&path=$path"
  }

  if ($PSBoundParameters.ContainsKey('order')) {
    $GAPI_COMMITS += "&order=$order"
  }

  if ($PSBoundParameters.ContainsKey('since')) {
    $GAPI_COMMITS += "&since=$since"
  }

  if ($PSBoundParameters.ContainsKey('until')) {
    $GAPI_COMMITS += "&until=$until"
  }

  if ($first_parent) {
    $GAPI_COMMITS += "&first_parent=true"
  }

  if ($trailers) {
    $GAPI_COMMITS += "&trailers=true"
  }

  if ($with_stats) {
    $GAPI_COMMITS += "&with_stats=true"
  }

  return @(Invoke-RestMethod -headers $GLPT -uri $GAPI_COMMITS -method GET)
}

# get a single repository commit
function Get-GitlabCommit( [Parameter(Mandatory=$true)] [string] $project
                         , [Parameter(Mandatory=$true)] [string] $sha
                         )
{
  [string] $GAPI_COMMITS_ID = "$CI_API_V4_URL/projects/$project/repository/commits/$sha"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_COMMITS_ID -method GET)
}

# create a new repository commit
function New-GitlabCommit( [Parameter(Mandatory=$true)] [string] $project
                         , [Parameter(Mandatory=$true)]          $actions
                         , [Parameter(Mandatory=$true)] [string] $branch
                         , [Parameter(Mandatory=$true)] [string] $message
                         , [Parameter(Mandatory=$false)][string] $start_branch
                         , [Parameter(Mandatory=$false)][string] $start_sha
                         , [Parameter(Mandatory=$false)][string] $author_email
                         , [Parameter(Mandatory=$false)][string] $author_name
                         , [Parameter(Mandatory=$false)][switch] $force   # overwrite target branch
                         )
{
  $body_json = @{ "branch"         = "$branch";
                  "commit_message" = "$message";
                  "actions"        = @($actions);
                }

  if ($PSBoundParameters.ContainsKey('start_branch')) {
    $body_json += @{ "start_branch" = "$start_branch" }
  } elseif ($PSBoundParameters.ContainsKey('start_sha')) {
    $body_json += @{ "start_sha" = "$start_sha" }
  }

  if ($PSBoundParameters.ContainsKey('author_email')) {
    $body_json += @{ "author_email" = "$author_email" }
  }

  if ($PSBoundParameters.ContainsKey('author_name')) {
    $body_json += @{ "author_name" = "$author_name" }
  }

  if ($force) {
    $GAPI_COMMITS += "&force=true"
  }

  [string] $body = $body_json | ConvertTo-Json -depth 10

  [string] $GAPI_COMMITS = "$CI_API_V4_URL/projects/$project/repository/commits"
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_COMMITS -method POST -ContentType 'application/json' -body $body)
}

# revert a repository commit
function Undo-GitlabCommit( [Parameter(Mandatory=$true)] [string] $project
                          , [Parameter(Mandatory=$true)] [string] $branch
                          , [Parameter(Mandatory=$true)] [string] $sha
                          )
{
  [string] $GAPI_COMMITS_REVERT = "$CI_API_V4_URL/projects/$project/repository/commits/$sha/revert"
  [string] $body = @{ "branch" = "$branch" } | ConvertTo-Json -depth 10
  return (Invoke-RestMethod -headers $GLPT -uri $GAPI_COMMITS_REVERT -method POST -ContentType 'application/json' -body $body)
}
